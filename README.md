# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://lukatoni@bitbucket.org/lukatoni/stroboskop.git
```

Naloga 6.2.3:
https://bitbucket.org/lukatoni/stroboskop/commits/6acaa1cda6552b67a64393569fd2c11ebf490d2b

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/lukatoni/stroboskop/commits/d4e39e90c3420468513c68fff7b4a40254f96c81

Naloga 6.3.2:
https://bitbucket.org/lukatoni/stroboskop/commits/d899095b3d4c73060f97beca72c0a84f15e354ac

Naloga 6.3.3:
https://bitbucket.org/lukatoni/stroboskop/commits/2545ba6a0e19c8e6661ba09269f9f9b1e8c99049

Naloga 6.3.4:
https://bitbucket.org/lukatoni/stroboskop/commits/479257e7152f58f638f09f40284007eee9008c47

Naloga 6.3.5:

```
git checkout master
git merge izgled
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/lukatoni/stroboskop/commits/bc748f0cec6eb68dd771c66a78242212a87d0288

Naloga 6.4.2:
https://bitbucket.org/lukatoni/stroboskop/commits/f642dd6c3dc9e48035a6e4e98c466790fb5bd0d0

Naloga 6.4.3:
https://bitbucket.org/lukatoni/stroboskop/commits/380190e00976bf51b21388920d6f6121a500c7e4

Naloga 6.4.4:
https://bitbucket.org/lukatoni/stroboskop/commits/ec1af71e07f18043819febab4ff0125f9b163a96